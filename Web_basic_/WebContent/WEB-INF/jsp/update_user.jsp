<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>update</title>

<jsp:include page="header.jsp" flush="true" />

<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>


	<div class="container5">

		<h1 class="Update_Servlet">ユーザ情報更新</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form action="Update_Servlet" method="post">

			<input type="hidden" name="id" value="${user.id}">

			<div class="row">
				<div class="col-sm-2">
					<p class="update_user_text2">ログインID</p>
				</div>
				<div class="col-sm-10">
					<p class="update_user_name2">${user.loginId}</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="update_password_text">パスワード</p>
				</div>
				<div class="col-sm-9">
					<input type="password" size="48" name="login_password"
						class="update_password" value="${user.password}">
				</div>
			</div>

			<div class="repassword">
				<div class="row">
					<div class="col-sm-4">
						<p class="insert_repassword_text">パスワード(確認)</p>
					</div>
					<div class="col-sm-8">
						<input type="password" size="20" name="login_repassword"
							class="update_repassword">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="update_user_text">ユーザ名</p>
				</div>
				<div class="col-sm-9">
					<input type="text" size="48" name="login_username"
						class="update_user" value="${user.name}">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="insert_birth_text">生年月日</p>
				</div>
				<div class="col-sm-9">
					<input type="date" size="48" name="login_birth"
						class="update_birth">
				</div>
			</div>
			<input type="submit" class="btn btn-success stretched-link"
				value="更新" id="update" value="${user.birthDate}">
		</form>

		<a href="UserListServlet" class="back">戻る</a>

	</div>
</body>
</html>