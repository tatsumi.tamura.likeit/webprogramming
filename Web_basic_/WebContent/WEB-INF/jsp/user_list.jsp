<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>list</title>

<jsp:include page="header.jsp" flush="true" />

<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body class="body">

	<div class="container2">
		<h1 class="user_reference">ユーザ一覧</h1>

		<a href="Insert_Servlet" class="btn btn-lg btn-success btn-block"
			id="new">新規登録</a>


		<form action="UserListServlet" method="post">
			<div class="row">
				<div class="col-sm-2">
					<p class="login_text">ログインID</p>
				</div>
				<div class="col-sm-9">
					<input type="text" size="48" name="user_id" class="list_login">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="user_text">ユーザ名</p>
				</div>
				<div class="col-sm-9">
					<input type="text" size="48" name="user_name" class="list_user">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="birth_text">生年月日</p>
				</div>
				<div class="col-sm-4">
					<input type="date" size="4" name="login_birth1" class="list_birth"
						placeholder="年 / 月 / 日">
				</div>
				<div class="col-sm-1">
					<p>〜</p>
				</div>
				<div class="col-sm-3">
					<input type="date" size="4" name="login_birth2" class="list_birth"
						placeholder="年 / 月 / 日">
				</div>

			</div>
			<input type="submit" class="btn btn-secondary stretched-link"
				value="検索" id="search">
		</form>
	</div>

	<hr size="10" color="gray" class="line">

	<table class="table table-bordered">

		<thead class="thead-light">
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
		</thead>

		<tbody>

			<c:forEach var="user" items="${userLists}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>

						<div class="row">
							<div class="col-sm-4">
								<a class="btn btn-primary"
									href="Reference_Servlet?id=${user.id}">詳細</a>
							</div>

							<div class="col-sm-4">
								<c:if
									test="${userInfo.loginId == user.loginId || userInfo.loginId == 'admin'}">
									<a class="btn btn-success" href="Update_Servlet?id=${user.id}">更新</a>
								</c:if>
							</div>
							<div class="col-sm-4">
								<c:if test="${userInfo.loginId == 'admin'}">
									<a class="btn btn-danger" href="Delete_Servlet?id=${user.id}">削除</a>
								</c:if>
							</div>
						</div>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>


</body>
</html>