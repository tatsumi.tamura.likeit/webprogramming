<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert</title>

<jsp:include page="header.jsp" flush="true" />

<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class="container3">

		<h1 class="insert_new_user">ユーザ新規登録</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form action="Insert_Servlet" method="post">

			<div class="row">
				<div class="col-sm-2">
					<p class="insert_login_text">ログインID</p>
				</div>
				<div class="col-sm-9">
					<input type="text" size="48" name="login_id" class="insert_login">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="insert_password_text">パスワード</p>
				</div>
				<div class="col-sm-9">
					<input type="password" size="48" name="login_password"
						class="insert_password">
				</div>
			</div>

			<div class="repassword">
				<div class="row">
					<div class="col-sm-4">
						<p class="insert_repassword_text">パスワード(確認)</p>
					</div>
					<div class="col-sm-8">
						<input type="password" size="20" name="login_repassword"
							class="insert_repassword">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="insert_user_text">ユーザ名</p>
				</div>
				<div class="col-sm-9">
					<input type="text" size="48" name="login_username"
						class="insert_user">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<p class="insert_birth_text">生年月日</p>
				</div>
				<div class="col-sm-9">
					<input type="date" size="48" name="login_birth"
						class="insert_birth">
				</div>
			</div>
			<input type="submit" class="btn btn-success stretched-link"
				value="登録" id="insert">
		</form>

		<a href="UserListServlet" class="back">戻る</a>

	</div>
</body>
</html>