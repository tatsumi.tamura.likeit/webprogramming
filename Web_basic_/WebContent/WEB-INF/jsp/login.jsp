<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


</head>
<body>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div class="container1">

		<h1 class="login1">ログイン画面</h1>

		<form action="Login_Servlet" method="post">
			<p>
				ログインID <input type="text" name="login_id">
			</p>
			<p></p>
			<p>
				パスワード <input type="password" name="login_password">
			</p>
			<div class="login_button">
				<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
			</div>
		</form>
	</div>
</body>
</html>