<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body class="header">

	<div class="container_header">

		<div class="row">
			<div class="col-sm-5">

				<p>ようこそ  ${userInfo.name} さん</p>
			</div>
			<div class="col-sm-7">
				<a href="Logout_Servlet"><p class="logout">ログアウト</p></a>
			</div>
		</div>

	</div>
</body>
</html>