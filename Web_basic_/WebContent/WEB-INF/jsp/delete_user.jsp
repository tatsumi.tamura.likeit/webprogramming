<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>delete</title>

<jsp:include page="header.jsp" flush="true" />

<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="container6">

		<h1 class="delete_user">ユーザ消去確認</h1>


		<p>
			ログインID： ${user.loginId}<br> を本当に消去してよろしいでしょうか。
		</p>


		<div class="row">
			<div class="col-sm-5">
				<a href="UserListServlet">
					<button type="submit" value="キャンセル" id="cancel">キャンセル</button>
				</a>
			</div>

			<form action="Delete_Servlet" method="post">

				<div class="col-sm-7">
					<button type="submit" name="id" value="${user}" id="action">実行</button>
				</div>
			</form>
		</div>

	</div>
</body>
</html>