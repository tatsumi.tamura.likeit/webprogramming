<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>reference</title>

<jsp:include page="header.jsp" flush="true" />

<link rel="stylesheet" type="text/css" href="css/practice.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="container4">

		<h1 class="reference_user_reference">ユーザ情報詳細参照</h1>


			<div class="row">
				<div class="col-sm-6">
					<p class="reference_login_text">ログインID</p>
				</div>
				<div class="col-sm-6">
					<p class="reference_login">${user.loginId}</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<p class="reference_user_text">ユーザ名</p>
				</div>
				<div class="col-sm-6">
					<p class="reference_user_name">${user.name}</p>
			</div>
			</div>

			<div class="birthday">
				<div class="row">
					<div class="col-sm-6">
						<p class="reference_birth_text">生年月日</p>
					</div>
					<div class="col-sm-6">
						<p class="reference_birth">${user.birthDate}</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<p class="reference_submit_text">登録日時</p>
				</div>
				<div class="col-sm-6">
					<p class="reference_submit">${user.createDate}</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<p class="reference_update_text">更新日時</p>
				</div>
				<div class="col-sm-6">
					<p class="reference_update">${user.updateDate}</p>
				</div>
			</div>

		<a href="UserListServlet" class="back">戻る</a>

	</div>
</body>
</html>