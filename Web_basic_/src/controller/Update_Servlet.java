package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.UserBean;

/**
 * Servlet implementation class Update_Servlet
 */
@WebServlet("/Update_Servlet")
public class Update_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update_Servlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		UserBean onSession = (UserBean) session.getAttribute("userInfo");

		if (onSession == null) {
			response.sendRedirect("Login_Servlet");
			return;
		}

		String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserBean user = userDao.littleSearchAll_3(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String password = request.getParameter("login_password");
		String rePassword = request.getParameter("login_repassword");
		String userName = request.getParameter("login_username");
		String birth = request.getParameter("login_birth");

		if (!(password.equals(rePassword))) {

			request.setAttribute("errMsg", "パスワードとパスワード（確認）が等しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (userName == "" || birth == "") {

			request.setAttribute("errMsg", "全項目を入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// パスワードのMD5暗号化
		UserBean bean = new UserBean();
		String secret = bean.secret(password);

		UserDao user = new UserDao();

		if (password.equals("") && rePassword.equals("")) {

			user.updateInfo(id, userName, birth);

			response.sendRedirect("UserListServlet");

			return;
		}

		user.updateInfo(id, secret, userName, birth);

		response.sendRedirect("UserListServlet");

	}

}
