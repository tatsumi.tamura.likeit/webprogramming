package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import dao.UserDao;
import model.UserBean;

/**
 * Servlet implementation class Reference_Servlet
 */
@WebServlet("/Reference_Servlet")
public class Reference_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Reference_Servlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		UserBean onSession = (UserBean) session.getAttribute("userInfo");

		if (onSession == null) {
			response.sendRedirect("Login_Servlet");
			return;
		}

		String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserBean user = userDao.littleSearchAll_2(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_reference.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
