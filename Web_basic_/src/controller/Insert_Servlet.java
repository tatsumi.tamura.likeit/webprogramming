package controller;

import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.UserBean;

/**
 * Servlet implementation class Insert_Servlet
 */
@WebServlet("/Insert_Servlet")
public class Insert_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Insert_Servlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		UserBean onSession = (UserBean) session.getAttribute("userInfo");

		if (onSession == null) {
			response.sendRedirect("Login_Servlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert_user.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("login_password");
		String rePassword = request.getParameter("login_repassword");
		String userName = request.getParameter("login_username");
		String birth = request.getParameter("login_birth");

		UserDao dao = new UserDao();
		UserBean user = dao.littleSearchAll(loginId);

		if (user != null) {

			request.setAttribute("errMsg", "入力されたIDはすでに存在します");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (!(password.equals(rePassword))) {

			request.setAttribute("errMsg", "パスワードとパスワード（確認）が等しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (loginId == "" || password == "" || rePassword == "" || userName == "" || birth == "") {

			request.setAttribute("errMsg", "全項目を入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// パスワードのMD5暗号化
		UserBean bean = new UserBean();
		String secret = bean.secret(password);

		dao.addInfo(loginId, secret, userName, birth);

		response.sendRedirect("UserListServlet");

	}

}
