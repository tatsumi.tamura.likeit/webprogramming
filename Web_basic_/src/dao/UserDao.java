package dao;

import java.sql.Connection;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.UserBean;

public class UserDao {

//------------------------------ログイン確認---------------------------//

	public UserBean findByLoginInfo(String loginId, String secret) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, secret);
			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new UserBean(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

//------------------------------------SELECT-------------------------------------//

	public List<UserBean> findAll() {
		Connection conn = null;

		List<UserBean> userList = new ArrayList<UserBean>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO: 未実装：管理者以外を取得するようSQLを変更する

			String sql = "SELECT * FROM user WHERE id NOT IN(1)";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_data");
				String updateDate = rs.getString("update_data");

				UserBean user = new UserBean(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

//------------------------------------SEARCH-------------------------------------//
	public List<UserBean> searchAll(String loginId, String userName, String birth1, String birth2) {
		Connection conn = null;
		List<UserBean> userList = new ArrayList<UserBean>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			// ログインID
			if (!(loginId.isEmpty())) {
				sql += " AND login_id = '" + loginId + "'";
			}

			// ユーザ名
			if (!(userName.isEmpty())) {
				sql += " AND name LIKE '%" + userName + "%'";
			}

			// 誕生日1
			if (!(birth1.isEmpty())) {
				sql += " AND birth_date >= '" + birth1 + "'";
			}

			// 誕生日2
			if (!(birth2.isEmpty())) {
				sql += " AND birth_date <= '" + birth2 + "'";
			}

			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_data");
				String updateDate = rs.getString("update_data");

				UserBean user = new UserBean(id, loginId1, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}

		} catch (

		SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	// ---------------------------------SEARCH2(InsertServlet)-------------------------------------//

	public UserBean littleSearchAll(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "select * from user where login_id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_data");
			String updateDate = rs.getString("update_data");

			ps.executeQuery();

			UserBean user = new UserBean(id, loginId1, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (

		SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// ---------------------------------SEARCH3(ReferenceServlet)-------------------------------------//

	public UserBean littleSearchAll_2(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "select * from user where id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (!(rs.next())) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId2 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_data");
			String updateDate = rs.getString("update_data");

			UserBean user = new UserBean(id1, loginId2, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// ---------------------------SEARCH4(UpdateServlet,DeleteServlet)-------------------------------//

	public UserBean littleSearchAll_3(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "select * from user where id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (!(rs.next())) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId2 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_data");
			String updateDate = rs.getString("update_data");

			UserBean user = new UserBean(id1, loginId2, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

//------------------------------------INSERT-------------------------------------//

	public void addInfo(String loginId, String secret, String userName, String birth) {
//	public void addInfo(UserBean ub) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,birth_date,password)VALUES(?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, loginId);
			ps.setString(2, userName);
			ps.setString(3, birth);
			ps.setString(4, secret);

//			ps.setString(1, ub.getLoginId());
//			ps.setString(2, ub.getName());
//			ps.setString(3, ub.getBirthDate());
//			ps.setString(4, ub.getPassword());

			ps.executeUpdate();

			return;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return;
	}

//------------------------------------UPDATE-------------------------------------//

	public void updateInfo(String id, String secret, String userName, String birth) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ?, birth_date = ?, password = ? WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, userName);
			ps.setString(2, birth);
			ps.setString(3, secret);
			ps.setString(4, id);

			ps.executeUpdate();

			return;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return;
	}

//------------------------------------UPDATE_2-------------------------------------//

	public void updateInfo(String id, String userName, String birth) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, userName);
			ps.setString(2, birth);
			ps.setString(3, id);

			ps.executeUpdate();

			return;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return;

	}

//------------------------------------DELETE-------------------------------------//

	public void deleteInfo(String id) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, id);

			ps.executeUpdate();

			return;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return;
	}

}
